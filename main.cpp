#include <QCoreApplication>
#include <QtGui/qimagereader.h>
#include <QtGui/qfiledialog.h>
#include <QtGui/qimageiohandler.h>
#include <QTranslator>
#include <QProcess>
#include <QDir>
#include "opencv/cv.hpp"
//#include <opencv.hpp>
#include <iostream>
using namespace cv;
using namespace std;

void printType(Mat &mat) {
         if(mat.depth() == CV_8U)  printf("unsigned char(%d)", mat.channels());
    else if(mat.depth() == CV_8S)  printf("signed char(%d)", mat.channels());
    else if(mat.depth() == CV_16U) printf("unsigned short(%d)", mat.channels());
    else if(mat.depth() == CV_16S) printf("signed short(%d)", mat.channels());
    else if(mat.depth() == CV_32S) printf("signed int(%d)", mat.channels());
    else if(mat.depth() == CV_32F) printf("float(%d)", mat.channels());
    else if(mat.depth() == CV_64F) printf("double(%d)", mat.channels());
    else                           printf("unknown(%d)", mat.channels());
}

void printInfo(const char *prefix, Mat &mat) {
    printf("%s: ", prefix);
    printf("dim(%d, %d)", mat.rows, mat.cols);
    printType(mat);
    printf("\n");
}

void printInfo(Mat &mat) {
    printf("dim(%d, %d)", mat.rows, mat.cols);
    printType(mat);
    printf("\n");
}

enum ConvolutionType {
/* Return the full convolution, including border */
  CONVOLUTION_FULL,

/* Return only the part that corresponds to the original image */
  CONVOLUTION_SAME,

/* Return only the submatrix containing elements that were not influenced by the border */
  CONVOLUTION_VALID
};

Mat flip(Mat M){
    Mat result = M;
    cv::flip(M, result, -1); //-1 flip both axes
    return result;
}

void conv2(const Mat img, const Mat& kernel, ConvolutionType type, Mat& dest) {

    Mat source = img;
      if(CONVOLUTION_FULL == type) {
        source = Mat();
        const int additionalRows = kernel.rows-1, additionalCols = kernel.cols-1;
        copyMakeBorder(img, source, (additionalRows+1)/2, additionalRows/2, (additionalCols+1)/2, additionalCols/2, BORDER_CONSTANT, Scalar(0));
      }

      Point anchor(kernel.cols - kernel.cols/2 - 1, kernel.rows - kernel.rows/2 - 1);
      int borderMode = BORDER_CONSTANT;
      filter2D(source, dest, img.depth(), flip(kernel), anchor, 0, borderMode);

      if(CONVOLUTION_VALID == type) {
        dest = dest.colRange((kernel.cols-1)/2, dest.cols - kernel.cols/2)
                   .rowRange((kernel.rows-1)/2, dest.rows - kernel.rows/2);
    }
}

void print(Mat mat, int prec)
{
    for(int i=1; i<10; i++)
    {
        cout << "[";
        for(int j=1; j<10; j++)
        {
            cout << prec << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}

Mat mat2gray(const Mat& src)
{
    //Analogous of Matlab mat2gray
    Mat dst;
    normalize(src, dst, 0.0, 1.0, NORM_MINMAX);
    return dst;
}

Mat wallisFilter(Mat originalImage, float A, float B, unsigned short int kernelSize, unsigned short int  imposedLocalAverage, unsigned short int imposedLocalStandardDeviation ){

    Mat doubleOriginalImage;
    originalImage.convertTo(doubleOriginalImage, CV_32F);

    //Getting the local Average and standard deviation given the indicated kernekl

    //Method 1:
    //Mat nHood = Mat::ones(kernelSize, kernelSize, CV_32F);
    //I need to calculate the local image mean.
    //Originally I was using Matlab conv2 function, ported here.
    //See:
    //Mat localMean, sLocalMean;
    //conv2(doubleOriginalImage, nHood/kernelSize^2, ConvolutionType::CONVOLUTION_SAME , sLocalMean);
    //sLocalMean.convertTo(localMean, CV_32F);

    //Method 2:
    //Very smart alternative to matlab conv2 :
    //See: http://stackoverflow.com/questions/11456565/opencv-mean-sd-filter

    Mat localMean;
    blur(doubleOriginalImage, localMean, Size(kernelSize, kernelSize)); //Easier to compute this way
    Mat differentialImage;
    blur(doubleOriginalImage.mul(doubleOriginalImage), differentialImage, Size(kernelSize, kernelSize));
    Mat localStandardDeviation;
    cv::sqrt(differentialImage - localMean.mul(localMean), localStandardDeviation);

//    cv::namedWindow( "Mean", WINDOW_NORMAL );
//    cv::imshow( "Mean", mat2gray(localMean) );

//    cv::namedWindow( "Standard deviation", WINDOW_NORMAL );
//    cv::imshow( "Standard deviation", mat2gray(localStandardDeviation) );

//    Then I finally compute the Wallis function.
//    The Wallis function is computed using the formula described in
//    http://www.igp.ethz.ch/photogrammetry/education/lehrveranstaltungen/RSGISFS2011/Exercises/wallis_.pdf:
//    In Matlab:
//    r1= A.*grandStDev./(localStandardDeviation+(1-A));
//    r0= B.*grandAverage+(1-B-r1).*localMean;
//    out= bnim.*r1+r0;

    Mat r1 = A * imposedLocalStandardDeviation / (localStandardDeviation+(1-A));
    Mat r0 = B * imposedLocalAverage + localMean.mul(1-B-r1);
    Mat wallisFilteredImage = doubleOriginalImage.mul(r1) + r0;

    //return mat2gray(wallisFilteredImage);
    return wallisFilteredImage;
}

string slash2BackslashStrings(string s){
    auto it = std::find(s.begin(), s.end(), '/');
    while (it != s.end()) {
        auto it2 = s.insert(it, '\\');

        // skip over the slashes we just inserted
        it = std::find(it2+2, s.end(), '\\');
    }
    return s;
}

string doubleSlashStrings(string s){
    auto it = std::find(s.begin(), s.end(), '\\');
    while (it != s.end()) {
        auto it2 = s.insert(it, '\\');

        // skip over the slashes we just inserted
        it = std::find(it2+2, s.end(), '\\');
    }
    return s;
}

void replaceExt(string& s, const string& newExt) {

   string::size_type i = s.rfind('.', s.length());

   if (i != string::npos) {
      s.replace(i+1, newExt.length(), newExt);
   }
}

int executeSingleWallisFilter(char* imagePath, char* destinationImagePath, float wallisParameterA, float wallisParameterB, unsigned short int  kernelSize,unsigned short int imposedAverage,unsigned short int  imposedStDeviation)
{
    //char imagePath[]="C:\\Users\\Andrea\\Desktop\\_DSC3427_WB.tif";

    string ip = doubleSlashStrings((string)imagePath);
    Mat originalImage= imread(imagePath, CV_LOAD_IMAGE_GRAYSCALE ); //Load the image. In case it's RGB, it gets loaded in gray tones

    if(! originalImage.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find image: " << imagePath  << endl ;
        return -1;
    }

    Mat wallisFilteredImage = wallisFilter(originalImage,wallisParameterA,wallisParameterB,kernelSize,imposedAverage,imposedStDeviation);

    std::vector<int> qualityParameters;
    qualityParameters.push_back(CV_IMWRITE_JPEG_QUALITY);
    qualityParameters.push_back(100);




    string newFileNameWithTifExtension = (string)destinationImagePath;
    replaceExt(newFileNameWithTifExtension,"tif");

    cout <<  "Writing:" << newFileNameWithTifExtension << ": ";

    ////DGS:
    Mat wallisFilteredImageByte;
    wallisFilteredImage.convertTo(wallisFilteredImageByte, CV_8U);
    ///
    bool ok = imwrite( newFileNameWithTifExtension, wallisFilteredImageByte, qualityParameters);
    if (ok)
        cout <<  "Ok!" << endl;
    else
        cout <<  "Error writing result!" << endl;

//    cv::namedWindow( "Wallis filter", WINDOW_AUTOSIZE );
//    cv::imshow( "Wallis filter", wallisFilteredImage );

    //QProcess *process;
    QString cupa = QDir::currentPath();

    const char *cstr = newFileNameWithTifExtension.c_str();
    QString destFile(cstr);
    QString file =  "\"" + cupa +  "\\exiftool.exe\" -overwrite_original -tagsFromFile \"" + (QString)imagePath  + "\" \"" + destFile +"\"";
    file = file.replace("/","\\\\");

    cout << "Updating exif data in:" << newFileNameWithTifExtension <<endl;
    QProcess::execute(file.replace("/","\\\\"));

    //waitKey(0);
    return 0;
}

//    This is the original matlab function
//    function wallisFastAverageAndStDev3_0(imageIn,imageOut,strA,strB,strWindowSize,strAverage,strStDev,outFileType)
//    % let S be standard deviation for image
//    % let M be mean for image
//    % for each (x,y) pixel in image
//    % calculate local mean m and standard deviation s
//    % using an NxN neighborhoo
//    tic
//    A = str2num(strA);
//    B = str2num(strB);

//    grandAverage = str2num(strAverage);
//    grandStDev = str2num(strStDev);
//    outFileType = lower(outFileType);

//    [pathstr,fileNameIn,fileExtensionIn] = fileparts(imageIn);
//    disp(['I''m Wallising image ' imageIn])

//    i1=imread(imageIn);
//    if  size(i1,3)>=3
//        im = rgb2gray(i1);
//    else
//        im = i1;
//    end

//    %Se la finestra viene definita come auto viene calcolata automaticamente
//    if strcmpi(strWindowSize,'auto')==1
//        k =  4608*3072/41;
//        [y,x]=size(im);
//        windowSize = round(y*x/k);
//    else
//        windowSize = str2num(strWindowSize);
//    end

//    if mod(windowSize,2)==0
//        windowSize=windowSize+1;
//    end;

//    bnim = double(im);
//    nHood = ones(windowSize);

//    if nargin > 4
//        globalMean = grandAverage; %mean(bnim(:));
//        globalStandardDeviation = grandStDev; %std(bnim(:));
//    else
//        %Se l'utente non specifica media e deviazione standard
//        globalMean = 127;
//        globalStandardDeviation = 50;
//    end

//    localMean = conv2(bnim,ones(windowSize)/windowSize^2,'same');
//    localStandardDeviation=stdfilt(bnim,nHood);

//    r1= A.*grandStDev./(localStandardDeviation+(1-A));
//    r0= B.*grandAverage+(1-B-r1).*localMean;
//    out= bnim.*r1+r0;


//    %Ricopia i bordi dell'immagine originale
//    border = round(windowSize / 2)+1;
//    % bhh = im(1:border,1:size(im,2));
//    % bhl = im(size(im,1)-border:size(im,1),1:size(im,2));
//    % bvl = im(1:size(im,1),1:border);
//    % bvr = im(1:size(im,1),size(im,2)-border:size(im,2));

//    out(1:border,1:size(im,2)) = im(1:border,1:size(im,2));
//    out(size(im,1)-border:size(im,1),1:size(im,2)) = im(size(im,1)-border:size(im,1),1:size(im,2));
//    out(1:size(im,1),1:border) = im(1:size(im,1),1:border);
//    out(1:size(im,1),size(im,2)-border:size(im,2)) = im(1:size(im,1),size(im,2)-border:size(im,2));


//    if strcmp(outFileType,'tif')==1
//        imwrite(uint8(out),imageOut,outFileType,'Compression','lzw');
//    elseif strcmp(outFileType,'jpg')==1
//        imwrite(uint8(out),imageOut,outFileType,'Quality',100);
//    end
//    toc
//    tic
//    disp(['Copying Exif metaTags from ' imageIn ' to ' imageOut] )
//    command=['exiftool -overwrite_original -tagsFromFile "' imageIn '" "' imageOut '"'];
//    system(command);
//    toc
//    return



//int main(int argc, char* argv[]){

//    // Check the number of parameters
//    if (argc < 9) {
//        cerr << "Usage: " << argv[0] << " NAME" << endl;
//        cout <<  "Wallis fileExtensionFilter sourceProcess destinationFolder WallisParameterA WallisParameterB kernelSize Average StDev" << endl ;
//        cout <<  "EXAMPLE: Wallis *.jpg C:\source C:\dest 1 .2 41 127 50" << endl ;
//        waitKey(0);
//        return -1;
//    }

//    float wallisParameterA, wallisParameterB;
//    unsigned short int imposedAverage, imposedStDeviation, kernelSize;
//    const char* fileType2Process;
//    const char* folder2Process;
//    const char* destinationFolder;

//    fileType2Process =          argv[1];
//    folder2Process =            argv[2];
//    destinationFolder =         argv[3];
//    wallisParameterA =          atof( argv[4]);
//    wallisParameterB =          atof( argv[5]);
//    kernelSize =                atoi( argv[6]);
//    imposedAverage =            atoi( argv[7] ) ;
//    imposedStDeviation =        atoi( argv[8]);


//    QStringList nameFilter(fileType2Process);
//    QDir directory(folder2Process);
//    QStringList txtFilesAndDirectories = directory.entryList(nameFilter);

//    char* imagePath;

//    //Create the destination folder if doesn't exists
//    if (! QDir(destinationFolder).exists()){
//        QDir().mkdir(destinationFolder);
//    }


//    foreach (QString impa,txtFilesAndDirectories){
//        QByteArray ba = impa.toLocal8Bit();
//        imagePath =ba.data();

//        char sourceImage[255];
//        strcpy_s(sourceImage,folder2Process);
//        strcat_s(sourceImage,"\\");
//        strcat_s(sourceImage,imagePath);

//        char destinationImage[255];
//        strcpy_s(destinationImage,destinationFolder);
//        strcat_s(destinationImage,"\\");
//        strcat_s(destinationImage,imagePath);

//        executeSingleWallisFilter(sourceImage,
//                                  destinationImage,
//                                  wallisParameterA,
//                                  wallisParameterB,
//                                  kernelSize,
//                                  imposedAverage,
//                                  imposedStDeviation);
//    }
int main(int argc, char* argv[]){
    float wallisParameterA, wallisParameterB;
    unsigned short int imposedAverage, imposedStDeviation, kernelSize;
    const char* fileType2Process;
    const char* folder2Process;
    const char* destinationFolder;

    QStringList txtFilesAndDirectories;

    if(argc == 9){
        fileType2Process =          argv[1];
        folder2Process =            argv[2];
        destinationFolder =         argv[3];
        wallisParameterA =          atof( argv[4]);
        wallisParameterB =          atof( argv[5]);
        kernelSize =                atoi( argv[6]);
        imposedAverage =            atoi( argv[7] ) ;
        imposedStDeviation =        atoi( argv[8]);


        QStringList nameFilter(fileType2Process);
        QDir directory(folder2Process);
        txtFilesAndDirectories = directory.entryList(nameFilter);

        char* imagePath;

        //Create the destination folder if doesn't exists
        if (! QDir(destinationFolder).exists()){
            QDir().mkdir(destinationFolder);
        }


        foreach (QString impa,txtFilesAndDirectories){
            QByteArray ba = impa.toLocal8Bit();
            imagePath =ba.data();

            char sourceImage[255];
            strcpy_s(sourceImage,folder2Process);
            strcat_s(sourceImage,"\\");
            strcat_s(sourceImage,imagePath);

            char destinationImage[255];
            strcpy_s(destinationImage,destinationFolder);
            strcat_s(destinationImage,"\\");
            strcat_s(destinationImage,imagePath);

            executeSingleWallisFilter(sourceImage,
                                      destinationImage,
                                      wallisParameterA,
                                      wallisParameterB,
                                      kernelSize,
                                      imposedAverage,
                                      imposedStDeviation);
        }
    }else if (argc == 8){
        char* imagePath =            argv[1];
        destinationFolder =         argv[2];
        wallisParameterA =          atof( argv[3]);
        wallisParameterB =          atof( argv[4]);
        kernelSize =                atoi( argv[5]);
        imposedAverage =            atoi( argv[6] ) ;
        imposedStDeviation =        atoi( argv[7]);

        //Create the destination folder if doesn't exists
        if (! QDir(destinationFolder).exists()){
            QDir().mkdir(destinationFolder);
        }

        QFileInfo imageFile(imagePath);
        char destinationImage[255];
        strcpy_s(destinationImage,destinationFolder);
        strcat_s(destinationImage,"\\");
        strcat_s(destinationImage,imageFile.fileName().toAscii());

        executeSingleWallisFilter(imagePath,
                                  destinationImage,
                                  wallisParameterA,
                                  wallisParameterB,
                                  kernelSize,
                                  imposedAverage,
                                  imposedStDeviation);

    }else{
        cerr << "Usage: " << argv[0] << " NAME" << endl;
        cout <<  "Wallis fileExtensionFilter sourceProcess destinationFolder WallisParameterA WallisParameterB kernelSize Average StDev" << endl ;
        cout <<  "EXAMPLE: Wallis *.jpg C:\source C:\dest 1 .2 41 127 50" << endl ;
        waitKey(0);
        return -1;
    }

}
