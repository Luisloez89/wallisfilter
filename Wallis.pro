#-------------------------------------------------
#
# Project created by QtCreator 2015-03-03T17:57:35
#
#-------------------------------------------------

# ensure one "debug_and_release" in CONFIG, for clarity...
debug_and_release {
    CONFIG -= debug_and_release
    CONFIG += debug_and_release
}

# ensure one "debug" or "release" in CONFIG so they can be used as
# conditionals instead of writing "CONFIG(debug, debug|release)"...
CONFIG(debug, debug|release) {
    CONFIG -= debug release
    CONFIG += debug
    }
CONFIG(release, debug|release) {
        CONFIG -= debug release
        CONFIG += release
}

QT       += core

QT       -= gui

TARGET = Wallis
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

INCLUDEPATH += ../../libs/openCV3_1_vs10_x64_2/include
LIBS += -L../../libs/openCV3_1_vs10_x64_2/x64/vc10/lib \
    -L../../libs/openCV3_1_vs10_x64_2/x64/vc10/bin

debug{
   LIBS += -lopencv_core310d \
           -lopencv_highgui310d \
           -lopencv_imgproc310d \
           -lopencv_imgcodecs310d
    win32-*{
        !contains(QMAKE_TARGET.arch, x86_64) {
            DESTDIR = build/debug
        }else{
            DESTDIR = build64/debug
        }
    }
}else{
    LIBS += -lopencv_core310 \
        -lopencv_highgui310 \
        -lopencv_imgproc310 \
        -lopencv_imgcodecs310
    win32-*{
        !contains(QMAKE_TARGET.arch, x86_64) {
            DESTDIR = build/release
        }else{
            DESTDIR = build64/release
        }
    }
}
